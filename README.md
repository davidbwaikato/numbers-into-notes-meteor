# README #

A Meteor-based version of the NumbersToNotes project developed by Dave De Roure at the Oxford eResearch Centre (OeRC).

### Assumptions ###

* You have [Meteor](http://www.meteor.com) installed
* To use the mobile option, you have the necessary SDK installed

### How do I get set up? ###

With meteor installed, to run the desktop version of this project 
nothing else needs to be done.  Just skip down to the To Run notes
below.  

To operate on a mobile enviroment, or make changes to the resources folder
(for icons and splash pages used on mobile devices), then
you'll want to edit SETUP.bash, where commonly needed environment 
variables are located.  The lines to edit are near the top of the file.

### To Run ###

    meteor run

And then in a web browser, visit http://localhost:3000


To run on the Android emulator:

    meteor run android

To run on a Android device (plug in to host, developer options switched on)

    meteor run android-device

### Icons and Splash Page ###

In the 'resources' folder you will find the files used by a range of
mobile devices for the start-up splash page, and icon.  These images
are automatically generated from a source SVG file also located in
this folder, by respectively running GENERATE-SPLASH.pl and GENERATE-ICON.sh.
You need to have the SVG editor Inkscape installed: either set your
PATH enviroment variable to include the *lowercase* 'inkscape' command-line
utility, or else edit INKSCAPE_BIN in SETUP.bash
according to where this has been installed.


 