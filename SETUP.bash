
# Set values as either 0 or 1 accordingly
NIN_ACTIVATE_ANDROID=1
NIN_ACTIVATE_IOS=0      % not currently implemented!
NIN_ACTIVATE_INKSCAPE=1

# Set the following according to your local environment
export ANDROID_HOME="/Users/davidb/Library/Android/sdk/"
export INKSCAPE_BIN="/Applications/Inkscape.app/Contents/Resources/bin"

echo "***"
echo "* Setting up Numbers Into Notes (Meteor)"
echo "***"
echo "*"

echo "+ Checking for Meteor installation: "
meteor --version
if [ $? != 0 ] ; then
  echo "Error: Failed to find 'meteor'" >&2
else 

  # To generate an Android version, need 'ANDROID_HOME' to point
  # to where it is on the file-system

  if [ $NIN_ACTIVATE_ANDROID = "1" ] ; then
    if [ ! -d "$ANDROID_HOME" ] ; then
      echo "Warning: Failed to find '$ANDROID_HOME' directory" >&2
    else 
      echo "+ Adding in ANDROID_HOME 'tools' and 'platform-tools' into path"
      export PATH="$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH"
    fi
  fi


  # Using Inkscape only needed if regenerating the icons and splash pages
  # (used in mobile versions) in 'resources/'

  if [ $NIN_ACTIVATE_INKSCAPE = "1" ] ; then
    if [ "x$INKSCAPE_BIN" != "x" ] ; then
      if [ ! -d "$INKSCAPE_BIN" ] ; then
        echo "Warning: Failed to find '$INKSCAPE_BIN' directory" >&2
      else 
        echo "+ Adding in Inkscape 'bin' path"
        export PATH="$INKSCAPE_BIN:$PATH"
      fi
    else
      # Determine if it is already on your path
      inkscape --version 2>&1 > /dev/null
      if [ $? != "0" ] ; then
        echo "Warning: Failed to find 'inkscape' on PATH"
        echo "         Needed if running REGENERATE-ICONS.sh or REGENERATE-SPLASH.pl"
      fi
    fi
  fi

  echo "*"
  echo "* Now, for Desktop app mode: "
  echo "*  meteor"

  if [ $NIN_ACTIVATE_ANDROID = "1" ] ; then
    echo "* Alternatively, for Android:" 
    echo "*   meteor add-platform android"
    echo "* if not already done so, then:"
    echo "*   meteor run android-device"
    echo "*"
    echo "* Debugging: "
    echo "*   adb devices"
    echo "* to test to see you plugged in device is there"
    echo "* Or:"
    echo "*   meteor run android"
    echo "* to start up an emulator version"
    echo "***"
  fi

fi
