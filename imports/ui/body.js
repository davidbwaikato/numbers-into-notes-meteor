import { Template } from 'meteor/templating';

$(document).ready(function(){
	var carousel_div = $("#NumToNoteCarousel");
	var about_div = $("#about-div");
	about_div.hide();
	$("#about-button").click(function(){
		carousel_div.slideToggle();
		about_div.slideToggle();

	    });
	$("#start-over-button").click(function(){
		carousel_div.slideToggle();
		about_div.slideToggle();

	    });
    });

import { Store } from '../api/store.js';

import './body.html';

import './intro.html';

import './generate.html';
import './reduce.html';
import './play.html';
import './export.html';
import './publish.html';

import './about.html';
import './footer.html';


Template.compositions.helpers({
	// Store your songs, e.g. 'Fibonacci Fun', 'Power On', and 'Ratio Rush'
	store() {
	    return Store.find({});
	},
	hasContent: function() {
	    return Store.find().count() > 0;
	},
    });

Template.composition.events({
	'click .load'() {
	    var item = Store.findOne(this._id);
	    $('#notestext').val(item.notes);
	    $('#NumToNoteCarousel').carousel(4);
	},
	    'click .delete'() {
		var item = Store.findOne(this._id);
		var status = confirm("Delete '" + item.title + "' Are you sure?");
		if (status == true) {
		    Store.remove(this._id);
		}
	    },
    });

Template.publish.events({
	'submit .new-composition'(event) {
	    // Prevent default browser form submit
	    //event.preventDefault();
 
	    // Get value from form element
	    const target = event.target;
	    const text = target.text.value;
 
	    const notestext = $('#notestext').val();
	    // Insert a titled composition into the collection
	    Store.insert({
		    title : text,
			notes : notestext,
			createdAt: new Date(), // current time
			});
 
	    // Clear form
	    target.text.value = '';
	},
	    });