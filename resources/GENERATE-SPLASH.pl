#!/usr/bin/perl -w

use strict;
use Cwd 'abs_path';


# The following is now done in the SETUP.bash file
#my $INKSCAPE_BIN = "/Applications/Inkscape.app/Contents/Resources/bin";

#print "Adding Inkscape command-line utility to path\n";
#$ENV{'PATH'} = $INKSCAPE_BIN.":".$ENV{'PATH'};

my $input_file = "Ada_Lovelace_color-square-splash.svg";
my $input_x_dim = 734;
my $input_y_dim = 734;
my $input_filename = abs_path($input_file);

my $output_dims = [
  {'x_dim' => 320, 'y_dim' =>  480 },
  {'x_dim' => 320, 'y_dim' =>  480, 'scale' => 2 },
  {'x_dim' => 320, 'y_dim' =>  568 },
  {'x_dim' => 320, 'y_dim' =>  568, 'scale' => 2 },
  {'x_dim' => 480, 'y_dim' =>  800 },
  {'x_dim' => 720, 'y_dim' => 1280 },
  {'x_dim' => 768, 'y_dim' => 1024 },
  {'x_dim' => 768, 'y_dim' => 1024, 'scale' => 2  },
# landscape
  {'x_dim' =>  480, 'y_dim' => 320 },
  {'x_dim' =>  800, 'y_dim' => 480 },
  {'x_dim' => 1024, 'y_dim' => 768 },
  {'x_dim' => 1024, 'y_dim' => 768, 'scale' => 2  },
  {'x_dim' => 1280, 'y_dim' => 720 }
];

sub generate_splash
{
    my ($input_filename,
	$input_x_dim, $input_y_dim,
	$output_x_dim,$output_y_dim,
	$bproj_x_dim,$bproj_y_dim,
	$scale,$landscape) = @_;

    my $output_file = "splash/splash-${output_x_dim}x${output_y_dim}";
    if (defined $scale) {
	$output_file .= "\@${scale}x";
    }
    $output_file .= ".png";

    my $output_filename = abs_path($output_file);

    my $export_x_dim = $output_x_dim;
    my $export_y_dim = $output_y_dim;

    if (defined $scale) {
	$export_x_dim *= $scale;
	$export_y_dim *= $scale;
    }

    my $x_diff = $bproj_x_dim - $input_x_dim;
    my $y_diff = $bproj_y_dim - $input_y_dim; 

    my $xl = 0;
    my $xr = $input_x_dim-1;

    my $yt = 0;
    my $yb = $input_y_dim-1;

    if ($landscape) {
	my $x_gap = $x_diff/2;
	
	$xl -= $x_gap;
	$xr += $x_gap;
    }
    else {
	my $y_gap = $y_diff/2;
	
	$yt -= $y_gap;
	$yb += $y_gap;
    }


    print "--Generating: $output_file\n";
    print "  $export_x_dim x $export_y_dim \n";

    my $cmd = "inkscape";
    $cmd .= " \"--export-png=$output_filename\"";
    $cmd .= " --export-area=$xl:$yt:$xr:$yb";
    $cmd .= " --export-width=$export_x_dim";
    $cmd .= " --export-height=$export_y_dim";
    $cmd .= " --export-background=white";
    $cmd .= " --without-gui";
    $cmd .= " \"$input_filename\"";

    my $status = system($cmd);
    if ($status > 0) {
	print STDERR "Error: Failed to run command\n";
	print STDERR "  $cmd\n";
	exit 1;
    }



}



for my $od (@$output_dims) {
    my ($output_x_dim,$output_y_dim) = ($od->{'x_dim'},$od->{'y_dim'});
    my $scale = $od->{'scale'};

    my $landscape = ($output_x_dim > $output_y_dim);

    my $bproj_x_dim;
    my $bproj_y_dim;

    if ($landscape) {
	my $y_scale = $output_y_dim / $input_y_dim;
	print "*** y_scale = $y_scale\n";

	$bproj_x_dim = $output_x_dim / $y_scale; 
	$bproj_y_dim = $input_y_dim;
    }
    else {
	# portrait
	my $x_scale = $output_x_dim / $input_x_dim;

	$bproj_y_dim = $output_y_dim / $x_scale; 
	$bproj_x_dim = $input_x_dim;
    }

    print "output_dims  = $output_x_dim x $output_y_dim (land = $landscape)\n";
    print "back project = $bproj_x_dim x $bproj_y_dim\n";
    print "scale = $scale\n\n" if defined $scale;

    generate_splash($input_filename,
		    $input_x_dim,$input_y_dim, 
		    $output_x_dim,$output_y_dim,
		    $bproj_x_dim,$bproj_y_dim,
		    $scale, $landscape);

}



#for s in 60 76  ; do
#  d=$(($s*2))
#  echo "**Generating ${s}x${s}@2 icon"
#  inkscape \
#    --export-png=`pwd`/icon-${s}x${s}@2.png \
#    --export-width=$d --export-height=$d \
#    --without-gui \
#    `pwd`/Ada_Lovelace_color-square.svg
#done

