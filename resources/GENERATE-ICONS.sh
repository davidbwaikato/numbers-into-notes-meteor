#!/bin/bash

echo "Adding Inkscape command-line utility to path"
export PATH=/Applications/Inkscape.app/Contents/Resources/bin:$PATH

for s in 48 60 72 76 96 144 192 ; do
  echo "**Generating ${s}x${s} icon"
  inkscape \
    --export-png=`pwd`/icons/icon-${s}x${s}.png \
    --export-width=$s --export-height=$s \
    --export-background=white \
    --without-gui \
    `pwd`/Ada_Lovelace_color-square-icon.svg
done

for s in 60 76  ; do
  d=$(($s*2))
  echo "**Generating ${s}x${s}@2 icon"
  inkscape \
    --export-png=`pwd`/icons/icon-${s}x${s}@2x.png \
    --export-width=$d --export-height=$d \
    --export-background=white \
    --without-gui \
    `pwd`/Ada_Lovelace_color-square-icon.svg
done

